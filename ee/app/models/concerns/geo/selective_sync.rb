# frozen_string_literal: true

module Geo::SelectiveSync
  extend ActiveSupport::Concern

  def selective_sync?
    selective_sync_type.present?
  end

  def selective_sync_by_namespaces?
    selective_sync_type == 'namespaces'
  end

  def selective_sync_by_shards?
    selective_sync_type == 'shards'
  end
end
